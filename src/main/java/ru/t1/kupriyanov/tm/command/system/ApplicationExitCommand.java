package ru.t1.kupriyanov.tm.command.system;

import org.jetbrains.annotations.NotNull;

public final class ApplicationExitCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.exit(0);
    }

    @NotNull
    @Override
    public String getName() {
        return "exit";
    }

    @NotNull
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Close application.";
    }
}
